from django.contrib import admin
from .models import ProductoForm, Producto, Unidad, Productor, OfertaProducto, TipoProducto

# Register your models here.

admin.site.register(Producto);
admin.site.register(Unidad);
admin.site.register(Productor);
admin.site.register(OfertaProducto);
admin.site.register(TipoProducto);
