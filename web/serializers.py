from rest_framework import serializers

from web.models import Producto, Productor, OfertaProducto

class ProductoSerializer(serializers.ModelSerializer):

    unidad = serializers.StringRelatedField(many=False)
    tipoProducto = serializers.StringRelatedField(many=False)

    class Meta:
        model = Producto
        fields = ('nombres', 'descripcion', 'precio', 'foto', 'unidad', 'tipoProducto')

class ProductorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productor
        fields = ('nombres', 'apellidos', 'telefono', 'correo')

class OfertaSerializer(serializers.ModelSerializer):

    producto = ProductoSerializer(many=False, read_only=True)
    productor = ProductorSerializer(many=False, read_only=True)

    class Meta:
        model = OfertaProducto
        fields = ('id', 'cantidad', 'precio', 'fechaOferta', 'producto', 'productor')