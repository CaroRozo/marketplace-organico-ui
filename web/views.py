from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from datetime import datetime, date, timedelta

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

import json

from models import Producto, ProductoForm, BusquedaForm, Productor, OfertaProducto


# Create your views here.
from web.serializers import OfertaSerializer, ProductoSerializer, ProductorSerializer


def index(request):

    lista_productos = Producto.objects.filter()

    form = BusquedaForm()
    paginador = Paginator(lista_productos, 3)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_productos': lista_paginada, 'user_auth':request.user.is_authenticated(), 'form': form}

    return render(request, 'web/index.html', context)

def quienes(request):
    return render(request, 'web/quienes.html')

def nuestros(request):
    return render(request, 'web/nuestros.html')

def logout_user(request):
    logout(request)
    return redirect('index')

def login_user(request):

    if request.user.is_authenticated():
        return redirect('/web/Productor/home')

    mensaje = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('/web/Productor/home')
        else:
            messages.add_message(request, messages.ERROR, 'Nombre de usuario o clave no valido', 'danger')

    return render(request, 'web/index.html', {'mensaje': mensaje})

def home(request):
    if not request.user.is_authenticated():
        return redirect('index')
    else:
        return render(request, 'web/Productor/home.html')

@csrf_exempt
def new_Product_json(request):

    if request.method == 'POST':
        jsonProducto = json.loads(request.body)

        producto = Producto()
        producto.nombres = jsonProducto['nombres']
        producto.descripcion = jsonProducto['descripcion']
        producto.foto = jsonProducto['foto']
        producto.precio = jsonProducto['precio']
        producto.unidad = jsonProducto['unidad']
        producto.save()

        return redirect(reverse('index'))
    return redirect('index')

def consultaProductos(request):

    today = datetime.today()
    weekday = today.weekday()
    start_delta = timedelta(days=(weekday+8))
    start_of_week = today - start_delta

    #fecha_inicio = datetime.today()-timedelta(days=7)
    #fecha_fin = datetime.today()

    fecha_inicio = start_of_week
    fecha_fin = start_of_week + timedelta(days=7)

    if request.method == 'POST':
        if 'filtrar' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin),producto__precio=request.POST['precio'])
            semana_siguiente = False
        elif 'clear' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
            semana_siguiente = False
        elif 'semana2' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=((fecha_inicio + timedelta(days=7)), (fecha_fin + timedelta(days=7))))
            semana_siguiente = True
        elif 'semana1' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
            semana_siguiente = False
    else:
        lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
        semana_siguiente = False

    form = BusquedaForm()
    paginador = Paginator(lista_productos, 4)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'semana_siguiente': semana_siguiente,'lista_productos': lista_paginada, 'user_auth':request.user.is_authenticated(), 'form': form}

    return render(request, 'web/Producto/listProducts.html', context)


def ofertarProducto(request, id):
    if request.method == 'POST':

        producto = Producto.objects.get(id=id)
        current_user = request.user
        productor = Productor.objects.get(usuario=current_user)

        oferta = OfertaProducto()
        oferta.producto = producto
        oferta.productor = productor
        oferta.cantidad = request.POST.get('Cantidad')
        oferta.precio = request.POST.get('Precio')
        oferta.save()

        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Creada', 'success')
        return redirect('/web/Productor/ofertaproductos/1')

    else:
        producto = Producto.objects.get(id=id)

    context = { 'producto': producto }
    return render(request, 'web/Productor/newOferta.html', context)

def ofertaProductos(request, page):

    fecha_inicio = datetime.today().date()-timedelta(days=datetime.today().weekday())
    fecha_fin = fecha_inicio + timedelta(days=7)

    current_user = request.user
    productor = Productor.objects.get(usuario=current_user)
    lista_ofertas = OfertaProducto.objects.filter(productor=productor,fechaOferta__range=(fecha_inicio, fecha_fin))
    paginador = Paginator(lista_ofertas, 5)

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_ofertas': lista_paginada, 'user_auth':request.user.is_authenticated()}
    return render(request, 'web/Productor/listaOferta.html', context)

def productosOferta(request):

    if request.method == 'POST':
        lista_productos = Producto.objects.filter(precio=request.POST['precio'])
    else:
        lista_productos = Producto.objects.filter()

    form = BusquedaForm()
    paginador = Paginator(lista_productos, 4)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_productos': lista_paginada, 'user_auth':request.user.is_authenticated(), 'form': form}

    return render(request, 'web/Productor/listaProductos.html', context)


def ofertaProducto(request, id):
    if request.method == 'POST':
        oferta = OfertaProducto.objects.get(id=id)
        oferta.cantidad = request.POST.get('Cantidad')
        oferta.precio = request.POST.get('Precio')
        oferta.save()

        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Modificada', 'success')
        return redirect('/web/Productor/ofertaproductos/1')

    else:
        oferta = OfertaProducto.objects.get(pk=id)
        producto = oferta.producto

    context = { 'producto': producto, 'oferta' : oferta }
    return render(request, 'web/Productor/newOferta.html', context)


def ofertaProductoCancelar(request):
    if request.method == 'POST':
        oferta = OfertaProducto.objects.get(id=request.POST.get('oid'))
        oferta.delete()
        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Eliminada', 'success')

    return redirect('/web/Productor/ofertaproductos/1')


@api_view(['GET', 'PUT', 'DELETE'])
def oferta_detail(request, pk):
    """
    REST Consulta, actualiza o borra a una oferta producto.
    """
    try:
        oferta = OfertaProducto.objects.get(pk=pk)
    except OfertaProducto.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = OfertaSerializer(oferta)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = OfertaSerializer(oferta, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        oferta.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def oferta_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        oferta = OfertaProducto.objects.all()
        serializer = OfertaSerializer(oferta, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = OfertaSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def producto_detail(request, pk):
    """
    REST Consulta, actualiza o borra a un producto.
    """
    try:
        producto = Producto.objects.get(pk=pk)
    except Producto.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProductoSerializer(producto)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductoSerializer(producto, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        producto.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'POST'])
def producto_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        producto = Producto.objects.all()
        serializer = ProductoSerializer(producto, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ProductoSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def productor_detail(request, pk):
    """
    REST Consulta, actualiza o borra a un productor.
    """
    try:
        productor = Productor.objects.get(pk=pk)
    except Productor.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProductorSerializer(productor)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductorSerializer(productor, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        productor.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def productor_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        productor = Productor.objects.all()
        serializer = ProductorSerializer(productor, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ProductorSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)